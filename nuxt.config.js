
export default {
  mode: 'spa',
  /*
  ** Headers of the page
  */
  server: {
    port: 5000, // default: 3000
  },
  head: {
    title: process.env.npm_package_name || 'Covid19 Africa Cases | Cassavahub Limited',
    meta: [
      { charset: 'utf-8' },
      { name: 'viewport', content: 'width=device-width, initial-scale=1' },
      { hid: 'description', name: 'description', content: process.env.npm_package_description || '' }
    ],
    link: [
      { rel: 'icon', type: 'image/x-icon', href: '/favicon.ico' }
    ],
    script:[
      { src: '/vendor/jquery/jquery.min.js' },
      { src: '/vendor/bootstrap/js/bootstrap.bundle.min.js' },
      { src: '/vendor/vectormap/jquery.vmap.min.js' },
      { src: '/vendor/vectormap/maps/continents/jquery.vmap.africa.js' },
      // { src: '/js/africa/cassava.js' },
      { src: '/js/scripts.js' },
      { src: '/js/settings.js' },
      { src: '/js/quixnav-init.js' },
      { src: '/js/styleSwitcher.js' },
      // { src: '/js/highcharts/js/highcharts.js' },
      // { src: '/js/highcharts/js/exporting.js' },
      // { src: '/js/highcharts/js/export-data.js' },
      ]
  },
  /*
  ** Customize the progress-bar color
  */
  loading: { color: '#fff' },
  /*
  ** Global CSS
  */
  css: [
    '@/assets/css/style.css'
  ],
  /*
  ** Plugins to load before mounting the App
  */
  plugins: [
  ],
  /*
  ** Nuxt.js dev-modules
  */
  buildModules: [
  ],
  /*
  ** Nuxt.js modules
  */
  modules: [
  ],
  /*
  ** Build configuration
  */
  build: {
    /*
    ** You can extend webpack config here
    */
    extend (config, ctx) {
    }
  }
}
